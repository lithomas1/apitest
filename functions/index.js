// Referenced from https://github.com/firebase/functions-samples/blob/main/username-password-auth/functions/index.js
const functions = require('firebase-functions');

// The Firebase Admin SDK to access Firestore.
const admin = require('firebase-admin');
admin.initializeApp();

exports.login = functions.https.onRequest(async (req, res) => {
    // Allow CORS
    // TODO: In production, you should restrict the origin to your webiste name
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'GET, POST');
    res.set("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

    if (req.method === "OPTIONS") {
        // stop preflight requests here
        return res.status(204).send('');
    }

    // Grab the text parameter.
    const username = req.body.username;
    const password = req.body.password;
    // Check if request is well-formed
    if (username == null || password == null) {
        res.statusMessage = "Invalid request";
        return res.sendStatus(400); // Invalid request
    }
    // Check if username, password is a valid combo
    var db = admin.database();
    const userRef = db.ref(`users/${username}`);
    const ref = db.ref(`users/${username}/password`);

    let userData = await userRef.once("value");
    userData = userData.val();
    console.log(userData);
    if (userData == null) {
        res.statusMessage = "User does not exist";
        return res.sendStatus(404);
    }

     // TODO: Actually hash password
    let pwhash = await ref.once("value");
    pwhash = pwhash.val();

    console.log(pwhash);

    if (pwhash == password) {
        const firebaseToken = await admin.auth().createCustomToken(username);
        res.statusMessage = "Logged in";
        return res.status(200).json({token: firebaseToken}); // OK status code
    }
    if (pwhash == null) {
        res.statusMessage = "User does not have a password. Try logging in through Google instead";
        return res.sendStatus(405);
    }
    res.statusMessage = "Invalid username/password combination"
    return res.sendStatus(401); // Unauthorized status code
  });
exports.createAccount = functions.https.onRequest(async (req, res) => {
    // Allow CORS
    // TODO: In production, you should restrict the origin to your webiste name
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'GET, POST');
    res.set("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

    if (req.method === "OPTIONS") {
        // stop preflight requests here
        return res.status(204).send('');
    }

    var db = admin.database();
    const username = req.body.username;
    const password = req.body.password;
    if (username == null || password == null) {
        // Malformed request
        return res.sendStatus(400);
    }
    const ref = db.ref(`users/${username}`);
    let userData = await (await ref.once("value")).val();
    console.log(userData);
    if (userData != null) {
        res.statusMessage = "User already exists";
        return res.sendStatus(409); // Conflict status code
    }
    // Insert username and password to the table
    // TODO: Error handling?
    const usersRef = db.ref('users');
    usersRef.child(username).set({
        password: password,
    })
    const firebaseToken = await admin.auth().createCustomToken(username);
    return res.status(200).json({token: firebaseToken}); // OK status code
})
