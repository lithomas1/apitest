# API Endpoints

## Create an Account
https://us-central1-job-test-3ac69.cloudfunctions.net/createAccount

Request type: POST

Body Parameters:
- username, string, REQUIRED
- password, string, REQUIRED

HTTP Response Status Codes:
- 400, Bad Request
- 409, User Already Exists
- 200, User created successfully
    - A JSON object containing a Firebase JWT token will also be sent in the response body
        - e.g. ``{'token': 'aowefijaeoirhqoi3r'}``

## Login
https://us-central1-job-test-3ac69.cloudfunctions.net/login

Request type: POST

Body Parameters:
- username, string, REQUIRED
- password, string, REQUIRED

HTTP Response Status Codes:
- 400, Bad Request
- 401, Invalid username/password combo
- 404, User does not exist
- 405, User must login via Google
- 200, Logged in successfully
    - A JSON object containing a Firebase JWT token will also be sent in the response body
        - e.g. ``{'token': 'aowefijaeoirhqoi3r'}``
