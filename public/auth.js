import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.18.0/firebase-app.js'
import { getAuth, signInWithCustomToken, signInWithCredential, GoogleAuthProvider } from 'https://www.gstatic.com/firebasejs/9.18.0/firebase-auth.js'
import { getStorage, uploadString, uploadBytes, ref as ref_storage} from 'https://www.gstatic.com/firebasejs/9.18.0/firebase-storage.js'
import { getDatabase, set, child, ref as ref_db} from 'https://www.gstatic.com/firebasejs/9.18.0/firebase-database.js'

const firebaseConfig = {
    apiKey: "AIzaSyBfrtcLFE4C63_hnKgeECTzA86SJnJ7W5s",
    authDomain: "job-test-3ac69.firebaseapp.com",
    projectId: "job-test-3ac69",
    storageBucket: "job-test-3ac69.appspot.com",
    messagingSenderId: "761458122823",
    appId: "1:761458122823:web:01b3ece6cce9f6b05d5162",
    measurementId: "G-4YC42DFXCY"
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

var uid = null;

function redirectToLogIn() {
  alert("You must be signed in to access this page.");
  window.location.href = "./index.html";
}

async function signIn() {
  if (firebaseToken) {
    return signInWithCustomToken(auth, token)
        .then((result) => {
        // The signed-in user info.
        const user = result.user;
        console.log(`Logged in as ${JSON.stringify(user.uid)}`);
        uid = user.uid;
        // IdP data available using getAdditionalUserInfo(result)
        // ...
        }).catch((error) => {
          // TODO: More informative error, for now just redirect
          console.log(error.error);
          redirectToLogIn();
    });
  } else {
    // Sign in with Google
    return signInWithCredential(auth, token).then((result) => {
      const user = result.user;
      console.log(`Logged in as ${JSON.stringify(user.uid)}`);
      uid = user.uid;
    }).catch((error) => {
      // TODO: More informative error, for now just redirect
      console.log(error.error);
      redirectToLogIn();
    });
  }
}

var token = window.localStorage.getItem("token");
var firebaseToken = true;
if (token == null) {
  // Try looking for google-token (sign in with google)
  token = window.localStorage.getItem("google-token");
  if (token == null) {
    redirectToLogIn();
  }
  token = GoogleAuthProvider.credential(token);
  firebaseToken = false;
}
console.log(`Token is ${token}`);

await signIn();
console.log(`User id is ${uid}`);

const storage = getStorage(app);
const userRef = ref_storage(storage, `users/${uid}/photos`);

const db = getDatabase(app);
const dbUserImagesRef = ref_db(db,`users/${uid}/images`);

export function uploadImageFile(dataURL, imageName, coords, labelName) {
  // Uploads the image to Firebase Storage
  const fileRef = ref_storage(userRef, imageName);
  uploadString(fileRef, dataURL, "data_url").then((snapshot) =>{
    console.log(`Uploaded image ${imageName}`)
  });
  // Store the annotations, and image labels in Firebase RTD
  // Strip the extension, since firebase can't have . in path
  let imageNameNoExt = imageName.split('.')[0];
  console.log(`Label is ${labelName}`);
  set(child(dbUserImagesRef, imageNameNoExt), {
    "coords": coords,
    "label": labelName,
  });
}
