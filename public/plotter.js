import { uploadImageFile } from './auth.js'

// Global variables

var src = null; // Data URL of uploaded file
var src_name = null; // Name of uploaded file
var coords = null; // Coordinates of annotations on the image
var blurred = false; // Did we blur the image already?
// Image prediction accuracy
var n_images = 0;
var n_correct = 0;

// Constants
const myFillColor = "rgba(245,66,66,0.5)"; // Nice red color, w/ 50% transparency
const reader = new FileReader();

// Plotly.js Graph Configuration
const config = {
    responsive: true
}

// DOM Elements
const label_elem = document.getElementById("image-pred-text");
const stats_elem = document.getElementById("stats");
const img_label_div = document.getElementById("image-pred");

const yes_btn = document.getElementById("pred-correct");
const no_btn = document.getElementById("pred-wrong");

const segment_prompt = document.getElementById("segment-prompt");
const image_label = document.getElementById("image-label")

// Helper functions
async function getPredictionsFromHuggingFace(dataURL) {
    // Uses the Resnet @ https://huggingface.co/spaces/pytorch/ResNet space
    // to generate predictions
    const response = await fetch("https://pytorch-resnet.hf.space/run/predict", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
            data: [
                dataURL,
            ]
        })
    });
    const data = await response.json();
    return data["data"][0]["label"]; //return labels
}

async function labelImage(dataURL) {
    let label = await getPredictionsFromHuggingFace(dataURL);
    label_elem.innerText = `Predicted label: ${label}`
}

// Stats for number of images the labeler got right/wrong
function renderStats() {
    stats_elem.innerText = `Model Accuracy: ${n_correct}/${n_images}`;
    stats_elem.style.visibility = "visible";
}

// Callbacks

const img_upload_elem = document.getElementById('image-uploader');
img_upload_elem.onchange = () => {
    const file = img_upload_elem.files[0];
    if (file) {
        src_name = file.name;
        reader.readAsDataURL(file);
    }
}

yes_btn.onclick = () => {
    // Increment counters
    n_images += 1;
    n_correct += 1;
    renderStats();
    // Prevent multiple clicks
    yes_btn.disabled = true;
    no_btn.disabled = true;
}

no_btn.onclick = () => {
    n_images += 1;
    renderStats();
    // Prevent multiple clicks
    yes_btn.disabled = true;
    no_btn.disabled = true;
}

// Plot callback

reader.onload = async () => {
    src = reader.result;
    blurred = false; // Reset blurred
    const img_trace = [{
        source: src,
        type: "image"
    }]
    const layout = {
        autosize: true,
        dragmode: "drawclosedpath",
        shapes: [],
        title: {
            text: "Original Image (Unblurred)"
        }
    }
    await labelImage(src); // Label the image
    console.log(layout);
    Plotly.newPlot(img, img_trace, layout, config);
    img_label_div.style.visibility = "visible";
    segment_prompt.style.visibility = "visible"; // Prompt user to draw annotations

    // Processsing annotations
    // Relayout event triggers after user draws on image
    // NOTE: This needs to execute after Plotly.newPlot, otherwise
    // there is no "on" attribute
    img.on("plotly_relayout", async (data) => {
        // data is an SVG path, need to get back the coordinates
        // relative to image
        // Ignore other relayout events
        if ('shapes' in data) {
            // The SVG path returned should only have M (move to, at beginning), L(line), and Z (close path elements)
            // at least for the 'drawclosedpath' mode
            // ref https://dash.plotly.com/annotations#extracting-an-image-subregion-defined-by-an-annotation
            if (data["shapes"].length == 0) {
                // Means we just cleared the annotations
                // Don't need to do anything
                console.log("Not running, since no shapes")
                return;
            }
            const currFillColor = data["shapes"][0]["fillcolor"];
            if (currFillColor == myFillColor) {
                console.log("Not running, since triggered by fill color change")
                return;
            }
            segment_prompt.style.visibility = "hidden"; // Don't prompt once annotated

            var ogPath = data["shapes"][0]["path"];
            var path = ogPath.slice(1, -1); // start from 1 idx to remove M
            var pathComps = path.split("L");
            var coords = []; // Array of (x,y) coordinates
            for (let i = 0; i < pathComps.length; i++) {
                const coordStr = pathComps[i];
                coords.push(coordStr.split(","));
            }
            // Disable drawing more annotations
            // And fill the segmentation mask with a red color
            // User should press the clear button if they messed up
            const new_layout = {
                dragmode: false,
                shapes: [{
                    fillcolor: myFillColor,
                    path: ogPath
                }]
            }
            Plotly.relayout(img, new_layout);
            // Make the clear/save annotation buttons visible
            const annotationDiv = document.getElementById("annotation-div");
            annotationDiv.style.visibility = "visible";

            // Clear button functionality
            const clearBtn = document.getElementById("clear-btn");
            clearBtn.onclick = () => {
                // Use the original layout defined above
                // to reallow segmenting again
                // TODO: Prevent title from reverting to unblurred
                // when resetting on blurred
                const new_layout = {
                    shapes: [],
                    dragmode: "drawclosedpath"
                }
                console.log("Clearing annotations");
                Plotly.relayout(img, new_layout);
                // Hide the save/clear buttons
                annotationDiv.style.visibility = "hidden";
                segment_prompt.style.visibility = "visible";
                image_label.value = "";
            }

            // Save button funcitonality
            const saveBtn = document.getElementById("save-btn");
            saveBtn.onclick = async () => {
                // Upload image & annotations to firebase
                const labelText = image_label.value;
                if (labelText == null || labelText == "") {
                    alert("Please label the image before pressing save");
                    return;
                }
                uploadImageFile(src, src_name, coords, labelText);

                // Re-enable the buttons
                yes_btn.disabled = false;
                no_btn.disabled = false;

                // Hide the save/clear buttons and labels
                annotationDiv.style.visibility = "hidden";
                image_label.value = "";
                img_label_div.style.visibility = "hidden";

                // Apply blur with image-js
                if (blurred) {
                    // Don't blur again if blurred
                    console.log("Purging image")
                    Plotly.purge(img); // Clear the plots
                    return;
                }

                let blurred_img = await IJS.Image.load(src);
                blurred_img = blurred_img.gaussianFilter({"radius": 10, "sigma": 3}).toDataURL();
                const new_layout = {
                    "title" : {
                        text: "Original Image (Blurred)"
                    },
                    "shapes": [],
                    "dragmode": "drawclosedpath"
                }
                const new_img_trace = [{
                    source: blurred_img,
                    type: "image"
                }]
                Plotly.react(img, new_img_trace, new_layout);

                // Show prompts and labels
                await labelImage(blurred_img);
                img_label_div.style.visibility = "visible";
                segment_prompt.style.visibility = "visible";

                blurred = true;
                src = blurred_img;
                coords = null;
                // Name of blurred image when uploaded will
                // be <original name>-blurred
                // Strip away the ext, since it's not guaranteed to be the same
                // FWIW, image-js seems to always make pngs
                let [filename, ext] = src_name.split('.'); // TODO: Weird extensions/lack of extensions?
                src_name = `${filename}-blurred`;
            }
        }
    })
}




